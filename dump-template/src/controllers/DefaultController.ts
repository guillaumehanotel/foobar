import { Controller, HTTPMethod } from 'dump-foobar'
import DefaultModel from '../models/DefaultModel'

export default class DefaultController extends Controller {
    modelClass = DefaultModel

    static customRoutes = [
        {
            path: `/${DefaultController.getPath()}/hello`,
            httpMethod: HTTPMethod.GET,
            methodName: 'helloWorld',
        },
    ]

    static disabledRoutes = ['deleteList']

    async helloWorld(): Promise<void> {
        this.res.statusCode = 200
        this.res.write('hello world')
    }

};
