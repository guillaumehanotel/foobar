import { Column, HasMany, Model } from 'dump-foobar';

export default class DefaultModel extends Model {

  @Column()
  name!: string;

};
