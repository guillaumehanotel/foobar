import DefaultController from './DefaultController'
import { SwaggerController } from 'foobar-core'

export {
  DefaultController,
  SwaggerController
}
