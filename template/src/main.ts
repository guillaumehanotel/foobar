import * as controllerClasses from './controllers'
import * as modelClasses from './models'
import config from './config'
import { Foobar } from 'foobar-core'

const app = new Foobar(
  {
    controllerClasses,
    modelClasses,
    host: '127.0.0.1',
    port: 8000,
    config
  },
)

app.start()
