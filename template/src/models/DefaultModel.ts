import { Column, HasMany, Model } from 'foobar-core';

export default class DefaultModel extends Model {

  @Column()
  name!: string;

};
