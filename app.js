#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const cp = require('child_process')
const extract = require('extract-zip')

const DUMP_PROJECT = false

const primaryParameter = process.argv[2]
const secondaryParameter = process.argv[3]
const runDirectory = process.cwd()
const currentDirectory = __dirname

const templateZipPath = DUMP_PROJECT ? `${currentDirectory}/dump-template.zip` : `${currentDirectory}/template.zip`
const tmpProjectDirPath = DUMP_PROJECT ? `${runDirectory}/dump-template` : `${runDirectory}/template`

if (primaryParameter !== undefined) {
    if (primaryParameter === 'new' && secondaryParameter !== undefined) {
        const projectDirPath = runDirectory + '/' + secondaryParameter
        if(!isDirectoryExists(projectDirPath) && !isDirectoryExists(tmpProjectDirPath)) {
            extract(templateZipPath, { dir: runDirectory },  (error) => {
                if (error){
                    console.log(error)
                } else {
                    fs.rename(tmpProjectDirPath, projectDirPath, function(err) {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log('Building Foobar app...')
                            cp.exec('npm install', {
                                cwd: projectDirPath,
                                stdio:[0,1,2]
                            }, function () {
                                console.log('Project Build !')
                            })
                        }
                    })
                }
            })
        } else {
            console.log(`The directory '${secondaryParameter}' or 'template' already exists`)
        }

    } else {
        printHelp()
    }
} else {
    printHelp()
}

function createDirectoryIfNeeded(directory) {
    if (!fs.existsSync(secondaryParameter)) {
        fs.mkdirSync(secondaryParameter)
    } else {
        console.log('The directory already exists')
    }
}

function isDirectoryExists(directoryPath) {
    return fs.existsSync(secondaryParameter)
}

function printHelp() {
    console.log('$foobar new')
    console.log('$foobar help')
}
