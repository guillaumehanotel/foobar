# Installation &nbsp;

**With [node](http://nodejs.org) [installed](http://nodejs.org/en/download):**

```sh
# Get the latest stable release of Foobar
$ npm install foobar -g
```

## Your First Foobar Project

**Create a new app:**

```sh
# Create the scaffold
$ foobar new
```

**For help:**

```sh
# Show use command example
$ foobar help
```

## Team

FoobarTeam Our core team consists of:

| [![Guillaume HANOTEL](https://media-exp1.licdn.com/dms/image/C5603AQGD2g2cdgC1Hg/profile-displayphoto-shrink_800_800/0?e=1587600000&v=beta&t=XvgpWYqscRk_8CsKzLlaE0UFwVYZBDFDz36p9wjMWU8)](https://www.linkedin.com/in/guillaumehanotel/) | [![Clément CLOUX](https://media-exp1.licdn.com/dms/image/C4E03AQFVWYFDDjxQZQ/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=O9nEGOkzeZGw4wgLTwYcVYT0X5TP28wFOphQeN6idkM)](https://www.linkedin.com/in/cl%C3%A9ment-cloux-576923113/) | [![Louis SAGLIO](https://media-exp1.licdn.com/dms/image/C4D03AQFkKuN_ej-Ang/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=1k2Z5DmLlzjF6FxtkNzW8RyhJdkA9YcurrUXYyEfVB0)](https://www.linkedin.com/in/louis-saglio/) | [![Thomas MAURIN](https://media-exp1.licdn.com/dms/image/C4D03AQHMStE4vBfHTQ/profile-displayphoto-shrink_200_200/0?e=1587600000&v=beta&t=QWOD5_zGmXiC1JekcU83C49HLUqmJom4Ve2yH5O0OmI)](https://www.linkedin.com/in/thomas-maurin-79b123167/) |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                                                                                         [Guillaume HANOTEL](https://gitlab.com/guillaumehanotel)                                                                                          |                                                                                                 [Clément CLOUX](https://gitlab.com/clement.cloux)                                                                                                 |                                                                                         [Louis SAGLIO](https://gitlab.com/Louis-Saglio)                                                                                          |                                                                                                 [Thomas MAURIN](https://gitlab.com/Thorin10)                                                                                                 |
